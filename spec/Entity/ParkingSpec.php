<?php

namespace spec\App\Entity;

use App\Entity\Car;
use App\Entity\Parking;
use App\Exception\CarInvalidDimensions;
use App\Exception\NoAvailableSpace;
use PhpSpec\ObjectBehavior;

class ParkingSpec extends ObjectBehavior
{
    private const CORRECT_LENGTH = 5000;

    private const CORRECT_WIDTH = 2500;

    private const OVER_SIZE_LENGTH = 8000;

    private const OVER_SIZE_WIDTH = 4000;

    private const MAX_CAPACITY = 50;

    private const EXCEEDED_CAPACITY = 51;

    function it_should_have_no_cars_by_default()
    {
        $this->getCars()->shouldHaveCount(0);
    }

    function it_should_be_able_to_add_cars()
    {
        $this->beConstructedWith(50);

        $this->addCar(new Car(self::CORRECT_LENGTH, self::CORRECT_WIDTH));
        $this->addCar(new Car(self::CORRECT_LENGTH, self::CORRECT_WIDTH));
        $this->getCars()->shouldHaveCount(2);
    }

    function it_should_not_allow_to_add_over_sized_cars()
    {
        $this->shouldThrow(new CarInvalidDimensions())
            ->during('AddCar', [new Car(self::OVER_SIZE_LENGTH, self::OVER_SIZE_WIDTH)]);
    }

    function it_should_return_a_exception_if_full()
    {
        $this->beConstructedWith(self::MAX_CAPACITY);

        for ($i = 1; $i <= self::EXCEEDED_CAPACITY; $i++) {
            $this->addCar(new Car(self::CORRECT_LENGTH, self::CORRECT_WIDTH));
        }

        $this->shouldThrow(new NoAvailableSpace(sprintf('Max capacity of %s is reached.', self::MAX_CAPACITY)));
    }
}

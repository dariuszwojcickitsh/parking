<?php

declare(strict_types=1);

namespace spec\App\Entity;

use App\Entity\Car;
use App\Exception\CarInvalidLength;
use App\Exception\CarInvalidWidth;
use PhpSpec\ObjectBehavior;

class CarSpec extends ObjectBehavior
{
    private const CORRECT_LENGTH = 4000;

    private const CORRECT_WIDTH = 3000;

    private const WRONG_LENGTH = -2000;

    private const WRONG_WIDTH = 0;

    function it_should_return_exception_if_length_and_width_is_incorrect()
    {
        $this->beConstructedWith(self::WRONG_LENGTH, self::WRONG_WIDTH);
        $this->shouldThrow(CarInvalidLength::class)
            ->duringInstantiation();

        $this->beConstructedWith(self::CORRECT_LENGTH, self::WRONG_WIDTH);
        $this->shouldThrow(CarInvalidWidth::class)
            ->duringInstantiation();

        $this->beConstructedWith(self::WRONG_LENGTH, self::CORRECT_WIDTH);
        $this->shouldThrow(CarInvalidLength::class)
            ->duringInstantiation();
    }

    function it_should_instantiate_a_car_object_for_correct_values()
    {
        $this->beConstructedWith(self::CORRECT_LENGTH, self::CORRECT_WIDTH);

        $this->shouldBeAnInstanceOf(Car::class);
    }
}

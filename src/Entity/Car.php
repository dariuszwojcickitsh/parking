<?php

declare(strict_types=1);

namespace App\Entity;

use App\Exception\CarInvalidLength;
use App\Exception\CarInvalidWidth;

class Car
{
    /** @var int Length in milimeters */
    private $length;

    /** @var int Width in milimeters */
    private $width;

    public function __construct(int $length = 0, int $width = 0)
    {
        $this->guardLength($length);
        $this->guardWidth($width);

        $this->length = $length;
        $this->width = $width;
    }

    /**
     * @throws CarInvalidLength
     */
    private function guardLength(int $length): void
    {
        if ($length <= 0) {
            throw new CarInvalidLength();
        }
    }

    /**
     * @throws CarInvalidWidth
     */
    private function guardWidth(int $width): void
    {
        if ($width <= 0) {
            throw new CarInvalidWidth();
        }
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getWidth(): int
    {
        return $this->width;
    }


}

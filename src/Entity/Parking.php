<?php

namespace App\Entity;

use App\Exception\CarInvalidDimensions;
use App\Exception\NoAvailableSpace;

class Parking
{
    private const ALLOWED_LENGTH = 6000;

    private const ALLOWED_WIDTH = 3000;

    private $maxCapacity;

    /** @var Car[] */
    private $cars = [];

    public function __construct(int $maxCapacity = 0)
    {
        $this->maxCapacity = $maxCapacity;
    }

    public function getCars(): array
    {
        return $this->cars;
    }

    public function getMaxCapacity(): int
    {
        return $this->maxCapacity;
    }

    public function addCar(Car $car): void
    {
        if ($this->canAddCar($car)) {
            $this->cars[] = $car;
            return;
        }
    }

    /**
     * @throws NoAvailableSpace
     * @throws CarInvalidDimensions
     */
    private function canAddCar(Car $car): bool
    {
        if ($this->isFull() === false) {
            throw new NoAvailableSpace(sprintf('Max capacity of %s is reached.', $this->maxCapacity));
        }

        if ($car->getLength() <= self::ALLOWED_LENGTH && $car->getWidth() <= self::ALLOWED_WIDTH) {
            return true;
        }

        throw new CarInvalidDimensions();
    }

    private function isFull(): bool
    {
        if (count($this->getCars()) <= $this->maxCapacity) {
            return true;
        }

        return false;
    }
}

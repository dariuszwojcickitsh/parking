<?php

declare(strict_types=1);

namespace App\Exception;

class CarInvalidLength extends \Exception
{
}
